# SAM Broadcaster

SAM Broadcaster doesn't have a built-in feature for sending song metadata to remote machines but it does have PAL, a built-in programming language with which we can implement what we need.

We wrote a basic PAL script that logs songs to Spinitron over HTTP. It is available on [Bitbucket](https://bitbucket.org/Spinitron/sam-broadcaster).


### Instructions

You're going to install a PAL script file into your SAM Broadcaster app.

1. Using a web browser, find your API key in Spinitron. Go to Admin: Automation & API: Control Panel. Keep this browser tab open for use later.
1. In SAM Broadcaster, open the PAL Scripts window and click the **+** (plus sign) button in its tool bar.
1. A small dialog opens. Enter the script file name Spinitron.pal and click OK. SAM creates a new script and adds it to the list in the PAL Scripts window.
1. Double click the new line and SAM opens the PAL Scripting IDE window.
1. Now, using a web browser, navigate to our PAL script in Bitbucket [`Spinitron.pal`](https://bitbucket.org/spinitron/sam-broadcaster/raw/master/Spinitron.pal)
    - https://bitbucket.org/spinitron/sam-broadcaster/raw/master/Spinitron.pal
1. Copy-paste the script from the browser into PAL:
    - In the browser, click in tab with our PAL script, Ctrl-A to select all, Ctrl-C to copy.
    - In SAM, click in the main editor pane of the PAL Scripting IDE and Ctrl-V to paste.
1. In the File menu of the PAL Scripting IDE, choose Save and close the PAL Scripting IDE window.
1. Double click the Spinitron.pal line in the PAL Scripts window again to reopen the PAL Scripting IDE.
1. To configure your new script, find the line

        const apikey = 'YOURAPIKEY';
    and replace `YOURAPIKEY` with your API key that you find in the first step by copy-pasting it.
    
    For example, if your API key in Spinitron were `yJNlthJ13_9udxZlgV6PFMQb` then you would change the line to

        const apikey = 'yJNlthJ13_9udxZlgV6PFMQb';
        
    Save the PAL script again.

(If you are using the old version of Spinitron then you use a different PAL script, `Spinitron_old.pal`, and the config step is different because you don't have an API key. Instead, follow the instructions in the script on lines 18 thru 24.) 

To start/stop the script: click the Spinitron.pal line in the PAL Scripts window to select it and use the triangle/square buttons in the tool bar.

To debug or customize the script, double click the Spinitron.pal line in the PAL Scripts window. Responses from Spinitron should be in the Output pane.

Idk if pressing the Compile button at this stage makes any difference but I guess there's no harm clicking it?



## License

Copyright (c) 2013, 2018, Spinitron, LLC.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of Spinitron nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
