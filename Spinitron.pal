{**
 * Spinitron automation remote logging PAL Script for SAM Broadcaster.
 *
 * Run this script to log songs to Spinitron when a new song starts
 * playing in the active deck.  This revision was designed to log
 * playlists during SAM's automated overnight program.
 *
 * You should make changes in this script to configure it for use at
 * your station. Read all the comments for instructions.
 *
 * @repository https://bitbucket.org/spinitron/sam-broadcaster
 * @copyright Copyright (c) 2018 Spinitron, LLC. All rights reserved.
 * @author Original Author: Tom Worster <tom@spinitron.com>
 * @author Improved by: Eric Hines (WNMC) <ehines@nmc.edu>
 * @license 3-clause BSD license http://opensource.org/licenses/BSD-3-Clause
 *}


// Change this to yoyr API key. Find it in Spinitron Admin: Automation & API: Control Panel.
const apikey = 'YOURAPIKEY';


// Don't change this blok of declarations.
const urlbase = 'https://spinitron.com/api/spin/create-v1';
const minlen = 20;
var song     : TSongInfo;      // instantiate a TSongInfo object
var url      : String = '';    // in which the full URL is composed
var oldurl   : String = '';    // where we store the last song's data
var result   : String = '';    // result string from Spinitron
var length   : Integer = 0;    // song length in seconds
var CT       : DateTime = Now; // current time
var stoptime : DateTime;       // time to stop automation logging


// The following 7 conditions serve to stop the script at a preset
// time each day. If you want the script to run indefinetly (i.e.
// until you stop it manually) delete them and follow the instructions
// below.
if DayOfWeek(Now)= Monday
    then stoptime := T['06:00:00'];
if DayOfWeek(Now)= Tuesday
    then stoptime := T['06:00:00'];
if DayOfWeek(Now)= Wednesday
    then stoptime := T['06:00:00'];
if DayOfWeek(Now)= Thursday
    then stoptime := T['06:00:00'];
if DayOfWeek(Now)= Friday
    then stoptime := T['06:00:00'];
if DayOfWeek(Now)= Saturday
    then stoptime := T['08:00:00'];
if DayOfWeek(Now)= Sunday
    then stoptime := T['08:00:00'];

{* Main loop.
 * Use one of the following two while statements:
 * 1. To have the script run indefinetly use:
 *    while true do
 * 2. To make the script stop at a preset time each day use:
 *    while CT < stoptime do
 *}
while true do
begin
    CT := Now;

    {* NOTE: From SAM documentation...
     *
     * ActivePlayer: Returns the player object that is currently playing a track.
     * If DeckA is playing, it returns the DeckA player.
     * If DeckB is playing, it returns the DeckB player.
     * If both DeckA and DeckB are playing, then it returns DeckA.
     *   **So if SAM reads data during a crossfade it ALWAYS reads Deck A,
     *     even if Deck B is actually the next song. The delay after
     *     WaitForPlayCount at the end of the loop corrects this for crossfades
     *     up to some number of seconds. See below.**
     * If neither DeckA or DeckB are playing the result will be nil.
     *}
    If ActivePlayer <> nil then
    begin
        song := ActivePlayer.GetSongInfo;
        if song <> nil then begin
            length := Round(StrToInt(song['duration'])/1000);
            if length >= minlen then begin
                url := urlbase;
                url := url + '?access-token=' + apikey;
                url := url + '&aw=' + URLEncode(song['artist']);
                url := url + '&sn=' + URLEncode(song['title']);
                url := url + '&dn=' + URLEncode(song['album']);
                url := url + '&dr=' + URLEncode(song['AlbumYear']);
                url := url + '&dl=' + URLEncode(song['genre']);
                url := url + '&ln=' + URLEncode(song['label']);
                //url := url + '&sc=' + URLEncode(song['composer']);
                //url := url + '&se=' + URLEncode(song['comments']);
                url := url + '&sd=' + IntToStr(length);

                if url <> oldurl then
                begin
                    result := WebToStr(url);
                    writeLn(url);
                    writeLn(result); // Rudimentary logging.
                    oldurl := url;
                    url := '';
                end;
            end;
        end;
        PAL.WaitForPlayCount(1);
        PAL.WaitForTime('+00:00:03'); // Correct crossfades up to 3 sec long.
    end;
end;
